<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">
            <p>First name:</p>
        </label>
        <input type="text" id="first_name" name="first_name">
        <br>
        <label for="last_name">
            <p>Last name:</p>
        </label>
        <input type="text" id="last_name" name="last_name">
        <br>
        <label for="gender">
            <p>Gender:</p>
        </label>
        <input type="radio" id="gender" name="gender" value="male">Male <br>
        <input type="radio" id="gender" name="gender" value="female">Female <br>
        <input type="radio" id="gender" name="gender" value="other">Other
        <br>
        <label for="nationality">
            <p>Nationality:</p>
        </label>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
            <option value="other">Other</option>
        </select>
        <br>
        <label for="language">
            <p>Language Spoken :</p>
        </label>
        <input type="checkbox" id="language" name="language" value="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" id="language" name="language" value="english">English <br>
        <input type="checkbox" id="language" name="language" value="other">Other
        <br>
        <label for="bio">
            <p>Bio:</p>
        </label>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>